#! /usr/bin/env node
'use strict';

var EmailReport = require("./lib/emailReport");
var winston = require('winston');
var wOptions = {
    file: {
        level: 'info',
        filename: 'node-utsaevents.log',
        handleExceptions: false,
        json: false,
        maxsize: 5242880, //5MB
        maxFiles: 5,
        colorize: false
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
    }
};
var reportLogger = winston.createLogger({
  transports: [
    new winston.transports.File(wOptions.file),
    new winston.transports.Console(wOptions.console)
  ],
  exitOnError: true
});
reportLogger.info("emailReport.js: attempting email report");
/*
var options = {
  "r25ws" : {
    "username" : "username",
    "password" : "password",
    "ws_base_uri": "/r25ws/wrd/run/",
    "ws_base_url":"r25ws_server_domain_name"
  },
  "reports" : [
  {
      "name" : "Setup Worksheet - Tomorrow",
      "report_id" : 304,
      "report_run_id": 124283,
      "target_org_id" : 1333,
      "from_address" : "emcsevents@utsa.edu",
    }
],
  "logger" : consoleLogger
}
*/
var options = {
  "reports" : "worksheet-weekend.json",
  "logger" : reportLogger
}
var email = new EmailReport({});
email.run();

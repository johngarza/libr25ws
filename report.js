#! /usr/bin/env node
'use strict';

var winston = require('winston');
var Report = require("./lib/report");

console.log("run.js: attempting run report");

var runLogger = new (winston.Logger) ({
  transports: [
    new (winston.transports.Console)()
  ]
});

var options = {
  "r25ws" : {
    "username" : "your_user_name",
    "password" : "your_password",
    "ws_base_uri": "/r25ws/wrd/run/",
    "ws_base_url":"your.r25ws.server.domain.name"
  },
  "reports" : [
  {
      "name" : "Setup Worksheet - Tomorrow",
      "report_id" : 304,
      "report_run_id": 124283,
      "target_org_id" : 1333,
      "from_address" : "emcsevents@utsa.edu",
    }
],
  "logger" : runLogger
}

var optionsToo = {
  "r25ws" : 'r25ws.json',
  "reports" : 'worksheet-weekend.json',
  "logger" : runLogger
}

//var report = new Report(options);
//report.run();

var reportToo = new Report(optionsToo);
reportToo.run();

//var reportThree = new Report();
//reportThree.run();

/*
var mockReport = {
  "key": "results?request=799D515E-7E9B-4376-B1FC-EF3087CDFC61", 
  "type": "application/pdf",
  "message": "Operation in progress",
  "name": "Setup Worksheet - Tomorrow",
  "report_id": "304",
  "report_run_id": "124283",
  "target_org_id": "1333",
  "from_address": "john.garza@utsa.edu",
  "publication_date": "2014-01-29T17:00:43-06:00",
  "ws_base_url": "ut159630.utsarr.net",
  "ws_base_uri": "/r25ws/wrd/run/"
};

emailReport(mockReport);
*/

'use strict';

var lo = require("lodash");
var fs = require("fs");
var winston = require("winston");
var nconf = require('nconf');

var logOpts = {
  console: {
    level: 'debug',
    handleExceptions: false,
    json: false,
    colorize: true
  }
};

var exports = module.exports = {};

exports.standardParserOptions = function() {
  return {
    object: true,
    reversible: true,
    coerce: false,
    sanitize: false,
    trim: false
  }
};

exports.getLogger = function(options) {
  var logger = null;
  if (options['logger']) {
    logger = options['logger'];
  } else {
    logger = winston.createLogger({
      transports: [
	      new winston.transports.Console(wOptions.console)
      ],
      exitOnError: false
    });
  }
  return logger;
};

exports.getR25WSOptions = function(options) {
  var config = null;
  if (options['r25ws']) {
    if (lo.isString(options['r25ws'])) {
      nconf.file('config', {
        file: options['r25ws'],
        dir: 'config',
        search: true,
      });
    }
    if (lo.isPlainObject(options['r25ws'])) {
      nconf.file('config', {
        file: 'r25ws.json',
        dir: 'config',
        search: true,
      });
      nconf.set('r25ws', options['r25ws']);
    }
  } else {
    nconf.file('config', {
      file: 'r25ws.json',
      dir: 'config',
      search: true,
    });
  }
  config = nconf.get('r25ws');
  return config;
};

exports.getReportOptions = function(options, logger) {
  var reportOptions = null;
  if (options.reports) {
    if (lo.isString(options.reports)) {
      logger.info("report options is a string: %s", options.reports);
      logger.info(options.reports);
      nconf.file('config', {
        file: options.reports,
        dir: 'config',
        search: true,
      });
      reportOptions = nconf.get('reports');
      logger.info("*********************************");
      logger.info("report parsed %s", reportOptions);
      logger.info("*********************************");
    }
    if (lo.isArray(options.reports)) {
      reportOptions = options.reports;
    }
  } else {
    nconf.file('report', {
      file: 'reports.json',
      dir: 'config',
      search: true,
    });
    reportOptions = nconf.get('reports');
  }
  return reportOptions;
}

exports.optionsCheck = function(config, logger) {
  var check = false;
  //sanity checking, need the following to continue!
  if (!config['ws_base_url']) {
    logger.error('Invalid/Missing Parameters: ws_base_url');
    throw {
      name: 'Invalid/Missing Parameter(s)',
      message: 'ws_base_url not set, please pass it in with the options or set in your config.json file',
    };
  }
  if (!config['ws_base_uri']) {
    logger.error('Invalid/Missing Parameters: ws_base_uri');
    throw {
      name: 'Invalid/Missing Parameter(s)',
      message: 'ws_base_uri not set, please pass it in with the options or set in your config.json file',
    };
  }
  if (!config['username']) {
    logger.error('Invalid/Missing Parameters: username');
    throw {
      name: 'Invalid/Missing Parameter(s)',
      message: 'username not set, please pass it in with the options or set in your config.json file',
    };
  }
  if (!config['password']) {
    logger.error('Invalid/Missing Parameters: password');
    throw {
      name: 'Invalid/Missing Parameter(s)',
      message: 'password not set, please pass it in with the options or set in your config.json file',
    };
  }
  //sane defaults for missing optional parameters
  if (!config['port']) {
    config['port'] = 80;
  }
  if (!config['25live_base_url'] && config['ws_base_url']) {
    config['25live_base_url'] = config['ws_base_url'];
  }
  if (!config['r25live_base_uri'] && config['ws_base_url']) {
    config['25live_base_uri'] = '/25live/';
    check = true;
  }
  return check;
};

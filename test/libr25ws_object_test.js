'use strict';

var libr25ws = require("../lib/libr25ws");
var report = require("../lib/report");
var email = require("../lib/emailReport");
/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/
var mockOptions = {
  "r25ws" : {
    "username" : "fake_username",
    "password" : "fake_password",
    "ws_base_uri": "/r25ws/wrd/run/",
    "ws_base_url":"fake.r25ws.server.edu",
    "session": {}
  },
  "reports" : [    {
      "name" : "Totally Mock Report",
      "report_id" : 1,
      "report_run_id": 2,
      "from_address" : "mock_email@mock.edu",
      "target_org_id" : 3
    }
]
};
module.exports  = {
  setUp: function(done) {
    // setup here
    this.client = new libr25ws(mockOptions);
    this.report = new report(mockOptions);
    this.email = new email(mockOptions);
    done();
      },
  'libr25ws object tests': function(test) {
    test.ok(this.client.get);
    test.ok(this.client.post);
    test.ok(this.client.put);
    test.done();
  },
  'report object tests': function(test) {
    test.ok(this.report.checkResults);
    test.ok(this.report.reportDone);
    test.ok(this.report.reportMessage);
    test.ok(this.report.reportError);
    test.ok(this.report.handleReport);
    test.ok(this.report.run);
    test.done();
  },
  'emailReport object tests' : function(test) {
    test.ok(this.email.run);
    test.ok(this.email.emailReport);
    test.ok(this.email.generateFromTemplate);
    test.done();
  }
};

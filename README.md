# r25ws [![Build Status](https://secure.travis-ci.org/garza/libr25ws.png?branch=master)](http://travis-ci.org/garza/libr25ws)

Simple Resource25 WebSerivce libary for pulling data from CollegeNET's R25 WebService API.

## Getting Started
Install the module with: `npm install r25ws`

```javascript
var libr25ws = require('r25ws');
var options = {
  "r25ws" : {
    "username" : "your_r2ws_username",
    "password" : "your_r25ws_password",
    "ws_base_uri": "/r25ws/wrd/run/",
    "ws_base_url":"your_r25ws_server_domain.edu",
    "session": {}
  }
};

var client = new libr25ws();

client.get({ resource: "null.xml" }, function(jsonObj) {
  console.dir(jsonObj);
}, function(errorObj) {
  console.dir(errorObj);
});

```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2014 garza  
Licensed under the MIT license.

#! /usr/bin/env node
'use strict';

var EmailReport = require("./lib/emailReport");
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;

var wOptions = {
    file: {
        level: 'info',
        filename: './node-utsaevents.log',
        handleExceptions: false,
        json: false,
        maxsize: 5242880, //5MB
        maxFiles: 5,
        colorize: false
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
    }
};
var runLogger = createLogger({
  format: combine(
    timestamp(), prettyPrint()
  ),
  transports: [
    new transports.Console()
  ]
});

var options = {
    "reports" : "./config/utsaevents.json",
    "r25ws" : "./config/r25ws.json",
    "logger" : runLogger
};
var emailReport = new EmailReport(options);
emailReport.run();

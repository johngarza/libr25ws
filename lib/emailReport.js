'use strict';

var events = require('events');
var fs = require('fs');
var util = require('util');
var r25util = require('./util');
var nconf = require('nconf');
var xml2json = require('xml2json');
var winston = require('winston');
var lo = require('lodash');
var juice = require('juice');
var Report = require('./report');
var libr25ws = require('./libr25ws');

var EmailReport = function(options) {
  var self = this;
  var r25wsOptions = {};
  options = options || {};
  self.logger = r25util.getLogger(options);
  self.config = r25util.getR25WSOptions(options);
  r25wsOptions = self.config;
  self.logger.log("info", "options %s", options.r25ws);
  self.client = new libr25ws(options);
  events.EventEmitter.call(this);
  self.parserOptions = {
    object: true,
    reversible: true,
    coerce: false,
    sanitize: false,
    trim: false,
  };
  self.errors = [];

  self.generateFromTemplate = function(model, template) {
    var fileData = fs.readFileSync(template, 'UTF-8');
    var jobTemplate = lo.template(fileData);
    return jobTemplate(model);
  };

  self.saveReport = function(reportData) {
    var reportName = reportData['name'];
    var ftype = reportData['type'];

    self.logger.info('emailReport: saveReport: ');
    self.logger.info(lo.keys(reportData));

    if (ftype === 'application/pdf') {
      reportName = reportName + '.pdf';
    }
    if (ftype === 'text/html') {
      reportName = reportName + '.html';
    }
    var fname = '' + r25wsOptions['web_base_uri'] + reportName;
    var reportURL = 'http://' + reportData['ws_base_url'] + reportData['ws_base_uri'] + reportData['key'];
    var key = reportData['key'];
    var options = {
      resource: key + '&wait=5',
      outputFile: fname,
    };
    self.logger.info(reportURL);
    self.client.getStore(options, function(resContent, res) {
      if (resContent && res) {
        reportData['web_full_url'] = r25wsOptions['web_base_url'] + reportName;
        self.emailReport(reportData);
      } else {
        self.logger.error('no response content found from client.getStore');
      }
    });
  };

  self.emailReport =  function(report) {
    //var reportURL = 'http://' + report['ws_base_url'] + report['ws_base_uri'] + report['key'];
    //self.logger.info('emailReport: Async Report complete: ' + reportURL);
    var orgOptions = {
      resource: 'organization.xml?scope=extended&include=contacts&organization_id=' + report['target_org_id'],
    };
    self.client.get(orgOptions, function(conObj) {
      var contacts = xml2json.toJson(conObj, self.parserOptions);
      //console.dir(contacts);
      var organizationObj = contacts['r25:organizations']['r25:organization'];
      var contactsFound = [];
      var parsedContacts = contacts['r25:organizations']['r25:organization']['r25:contact'];
      if (!lo.isArray(parsedContacts)) {
        contactsFound.push (parsedContacts);
      } else {
        contactsFound = parsedContacts;
      }
      lo.forEach(contactsFound, function(itemObj) {
          self.logger.info('emailReport: attempting POST to contact.mail');
          var dataModel = {
            report_title: report['name'],
            pdf_location: report['web_full_url'],
            publication_date: report['publication_date'],
            organization_name: organizationObj['r25:organization_name'].$t,
          };
          var emailModel = {
            to_address: itemObj['r25:contact_email'].$t,
            from_address: report['from_address'],
            subject: '[R25@UTSA] ' + report['name'],
            publication_date: report['publication_date'],
          };
          self.logger.info('************************');
          console.dir(dataModel);
          //console.dir(itemObj);
          self.logger.info('************************');
          var htmlBody = self.generateFromTemplate(dataModel, 'tmpl/pdf-only.html');
          juice.juiceResources(htmlBody, { url: 'http://utsa.edu'}, function(err, body) {
            emailModel['text'] = '' + body;
            var payload = self.generateFromTemplate(emailModel, 'tmpl/r25-email.xml');
            var emailPostOptions = {
              resource: 'contact.mail?contact_id=' + itemObj['r25:contact_id'].$t,
              data: payload,
            };

            //self.logger.info(emailPostOptions['resource']);
            //            var emailer = new libr25ws();
            self.client.post(emailPostOptions,

            function(postSuccessContent) {
              if (postSuccessContent) {
                self.logger.info('emailReport: POST EMAIL SUCCESS: ' + itemObj['r25:contact_email'].$t);
              }  else {
                self.logger.error('no post success content recieved');
              }
            },

            function(postErrorObj) {
              self.logger.error('emailReport: POST FAILED:');
              self.logger.error(postErrorObj);

              //TODO: ERROR HANDLING!
            });
          });
        });
    },

    function(errorObj) {
      self.logger.error('emailReport: GET organization failed');
      self.logger.error(errorObj);
    });
  };

  self.run = function() {
    self.logger.info("run start");
    self.report = new Report(options);
    self.report.on('ReportDone', function(reportData) {
      self.saveReport(reportData);
      //delete reportData.data;
      //self.emailReport(reportData);
    });
    self.report.run();
  };
};

util.inherits(EmailReport, events.EventEmitter);
module.exports = EmailReport;

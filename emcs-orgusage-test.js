#! /usr/bin/env node
'use strict';

var EmailReport = require("./lib/emailReport");
var winston = require('winston');

var runLogger = new (winston.Logger) ({
  transports: [
    new (winston.transports.Console)()
  ]
});
var options = {
    "reports" : "emcs-orgusage-test.json",
    "logger" : runLogger
};

var emailReport = new EmailReport(options);
emailReport.run();


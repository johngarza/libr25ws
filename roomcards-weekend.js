#! /usr/bin/env node
'use strict';

var EmailReport = require("./lib/emailReport");
var winston = require('winston');
var wOptions = {
    file: {
	level: 'info',
	filename: '/home/garza/log/node-roomcards-weekend.log',
	handleExceptions: false,
	json: false,
	maxsize: 5242880, //5MB
	maxFiles: 5,
	colorize: false
    },
    console: {
	level: 'debug',
	handleExceptions: false,
	json: false,
	colorize: true
    }
};

var runLogger = winston.createLogger({
    transports: [
	new winston.transports.File(wOptions.file),
	new winston.transports.Console(wOptions.console)
    ],
    exitOnError: false
});

var options = {
    "reports" : "roomcards-weekend.json",
    "logger" : runLogger
};

var emailReport = new EmailReport(options);
emailReport.run();

//emailReport.init("roomcards.json");


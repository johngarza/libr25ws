'use strict';

var events = require('events');
var util = require('util');
var r25util = require('./util');
var nconf = require('nconf');
var winston = require('winston');
var lo = require('lodash');
var xml2json = require('xml2json');
var libr25ws = require('./libr25ws');
var ASYNC_WAIT = '&wait=15';

var Report = function(options) {
  var self = this;
  var r25wsOptions = {};
  var reportOptions = {};
  options = options || {};
  self.logger = r25util.getLogger(options);
  r25wsOptions = r25util.getR25WSOptions(options);
  self.config = r25wsOptions;
  self.client = new libr25ws(options);
  reportOptions = r25util.getReportOptions(options, self.logger);
  self.logger.info("report options %s", reportOptions);
  self.reports = reportOptions;

  events.EventEmitter.call(this);
  self.parserOptions = r25util.standardParserOptions();
  self.errors = [];
  self.checkResults = function(reportObj) {
    var key = reportObj['key'];
    var options = {
      resource: key + ASYNC_WAIT,
    };
    self.client.get(options, function(resContent, res) {
      var asyncMessage = null;
      var respJson = {};
      var result = {};
      var emit = false;
      try {
        respJson = xml2json.toJson(resContent, self.parserOptions);
        //self.logger.info('checkResults: JSON PARSE DONE');
        asyncMessage = respJson['r25:results']['r25:result']['r25:message'].$t;
        self.logger.info("async response was: %s", asyncMessage);
      } catch (e) {
        //response is not xml, maybe a pdf? send the data back in a wrapped object
        self.logger.info('checkResults: ' + key + ' : CAUGHT');
        result['key'] = key;
        result['data'] = resContent;
        result['type'] = res.headers['content-type'];
        self.logger.info('**************************');
        self.logger.info('**************************');
        self.logger.info('TYPE RECVD:');
        self.logger.info(result['type']);
        lo.merge(result, reportObj);
        self.emit('checkResultsDone', result);
        emit = true;
      }
      if (asyncMessage) {
        //non null async message, async is not done processing yet
        result['key'] = respJson['r25:results']['r25:result']['xl:href'];
        result['message'] = asyncMessage;
        //self.logger.info('checkResults: emit - checkResultsMessage');
        lo.merge(result, reportObj);
        self.emit('checkResultsMessage', result);
      } else {
        if (!emit) {
          //we haven't already reported this data earlier in the catch block
          //content is xml but not a r25:results response, report error? send data back in a wrapped object
          result['key'] = key;
          result['data'] = resContent;
          result['type'] = res.headers['content-type'];
          lo.merge(result, reportObj);
          self.emit('checkResultsDone', result);
        }
      }
    },
    function(errorObj) {
      var result = {};
      result['key'] = key;
      result['error'] = errorObj;
      lo.merge(result, reportObj);
      self.emit('checkResultsError', result);
    });
  };

  self.reportDone = function(report) {
    self.logger.info('Report DONE at URL: ' + r25wsOptions['ws_base_url'] + r25wsOptions['ws_base_uri'] + report['key']);
    console.dir(report);
    self.emit('ReportDone', report);
  };

  self.reportMessage = function(report) {
    self.checkResults(report);
  };

  self.reportError = function(report) {
    self.logger.error('REPORT ERROR - ' + report['key']);
    self.logger.error(report['error']);
  };

  self.handleReport = function(report) {
    var options = {
      resource: 'report.run?async=T&report_id=' + report['report_id'] + '&report_run_id=' + report['report_run_id'],
    };
    self.client.get(options, function(respObj) {
      var newKey = '';
      var respJson = {};
      respJson = xml2json.toJson(respObj, self.parserOptions);
      newKey = respJson['r25:results']['r25:result']['xl:href'];
      report['publication_date'] = respJson['r25:results']['pubdate'];
      report['ws_base_url'] = r25wsOptions['ws_base_url'];
      report['ws_base_uri'] = r25wsOptions['ws_base_uri'];
      report['key'] = newKey;
      self.checkResults(report);
    },
    function(errorObj) {
      self.errors.push({ new: 'Unable to run report', message: errorObj});
    });
  };

  self.run = function(reportObj) {
    if (reportObj) {
      self.logger.info('report.run: ' + reportObj);
      self.handleReport(reportObj);
    } else {
      //no report object given, lookup our nconf report settings
      lo.forEach(self.reports, function(report) {
        self.logger.info('Running report: ' + report['name']);
        self.handleReport(report);
      }, this);
    }
    self.on('checkResultsDone', self.reportDone);
    self.on('checkResultsError', self.reportError);
    self.on('checkResultsMessage', self.reportMessage);
  };
}; //end Report

util.inherits(Report, events.EventEmitter);
module.exports = Report;

#! /usr/bin/env node
'use strict';

var EmailReport = require("./lib/emailReport");
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;
var wOptions = {
    file: {
        level: 'info',
        filename: '/Users/rjq475/Development-utsa/libr25ws/node-eventsreport.log',
        handleExceptions: false,
        json: false,
        maxsize: 5242880, //5MB
        maxFiles: 5,
        colorize: false
    },
    console: {
        level: 'debug',
        handleExceptions: false,
        json: true,
        colorize: true
    }
};
var runLogger = createLogger({
  format: format.combine(
    format.splat(),
    format.simple()
  ),
  transports: [
    new transports.Console()
  ]
});

var options = {
    "reports" : "eventsreport.json",
    "logger" : runLogger,
    "r25ws" : "r25ws.json"
};

runLogger.info("logger instantiated");

var emailReport = new EmailReport(options);
emailReport.run();

'use strict';

var libr25ws = require("../lib/libr25ws");
var nock = require("nock");
var fs = require('fs');
/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/
var parserOptions = {
    object: true,
    reversible: true,
    coerce: false,
    sanitize: false,
    trim: false
};
var mockOptions = {
  "r25ws" : {
    "username" : "mock_username",
    "password" : "mock_password",
    "ws_base_uri": "/r25ws/wrd/run/",
    "ws_base_url":"r25ws.mock.edu"
  }
};

var doRequestOptions = { 
  'resource': 'login.xml',
  'port': 80,
  'ws_base_uri': '/r25ws/wrd/run/',
  'ws_base_url':'r25ws.mock.edu',
  method: 'GET'
};

var GETloginResponse = fs.readFileSync('./test/mock/GET_login.xml', "UTF-8");

module.exports  = {
  setUp: function(done) {
    // setup here
    this.client = new libr25ws(mockOptions);
    done();
  },
  'doRequest tests': function(test) {
    var r25ws = nock('http://r25ws.mock.edu').get('/r25ws/wrd/run/login.xml').reply(200, GETloginResponse);
    this.client.doRequest(doRequestOptions, function(result, response) {
      if (result === GETloginResponse) {
        test.ok(true, "doRequest: response to simple GET on login.xml correct");
      } else {
        test.ok(false, "doRequest: failed to get back simple GET response to login.xml");
      }
    }, function(error, response) {
      test.ok(false, "doRequest: encountered an error when attempting a simple GET to login.xml");
    });
    test.done();
  }
};
